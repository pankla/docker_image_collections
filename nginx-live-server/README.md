# README

Build nginx image with live broadcast function by using `nginx-http-flv-module`

## version

- `nginx`: 1.26.1 [nginx](https://nginx.org/en/download.html)
- `pcre`: pcre2-10.44 [pcre2](https://github.com/PCRE2Project/pcre2/releases)
- `openssl`: 3.3.1 [openssl net](https://www.openssl.org/source/)
- `zlib`: 1.3.1 [zlib net](https://zlib.net)
- `nginx-http-flv-module`: 1.2.11 [nginx-http-flv-module](https://github.com/winshining/nginx-http-flv-module)
- `ngx_http_consistent_hash`: cbaa354 [consistent hash](https://github.com/leev/ngx_http_geoip2_module)
- `ngx_http_geoip2_module`
- `ffmpeg`: 4.4.1

## base

`nginx-http-flv-module` include all functions of `nginx-rtmp-module`

base and reference

- [rtmp-hls-server](https://github.com/TareqAlqutami/rtmp-hls-server)
- [rtmp config](https://github.com/arut/nginx-rtmp-module/blob/master/README.md)

## pre

```bash
mkdir modules

cd modules
```

- 1，download the source code of nginx
- 2，download `nginx-http-flv-module`
- 3，download the static ffmpeg
- 4，download the pcre2

## dir

```text
.
├── CHANGELOG.md
├── modules
│   ├── build
│   │   ├── nginx-1.26.1
│   │   ├── nginx-http-flv-module-1.2.11
│   │   ├── ngx_http_consistent_hash
│   │   ├── openssl-3.3.1
│   │   ├── pcre2-10.44
│   │   └── pzlib-1.3.1
│   └── ffmpeg
├── conf
│   ├── live.conf
│   ├── nginx_no-ffmpeg.conf
│   ├── nginx_rtmp_minimal_no-stats.conf
│   └── nginx_with-ffmpeg.conf
├── docker-entrypoint.sh
├── Dockerfile
├── Dockerfile-with-ffmpeg
├── docs
│   └── settings.md
├── LICENSE
├── mirrors
│   └── sources.list
├── players
│   ├── dash.html
│   ├── hls_hlsjs.html
│   ├── hls.html
│   ├── rtmp_hls.html
│   └── rtmp.html
└── README.md
```

## build

```bash
docker build -f ./Dockerfile -t panla/nginx:230528 .
```

```bash
docker buildx use xxxxx

docker buildx build --platform linux/amd64,linux/arm64 --push -f ./Dockerfile -t panla/nginx:240706 .

```

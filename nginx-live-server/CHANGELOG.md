# CHANGELOG

[toc]

## 2023-05

- 2023-05-27
  - Upgrade: upgrade nginx from 1.22.0 to 1.24.0, nginx-http-flv-module from 1.2.10 to 1.2.11

## 2023-03

- 2023-03-21
  - Update: Nginx Compilation Modules
- 2023-03-19
  - Add: zlib openssl, pcre, source code into Compilation instructions
- 2023-03-09
  - Update: 更新 Dockerfile 指令，更新
- 2023-03-07
  - Fix: 修复 nginx-live-server Dockerfile 错误
- 2023-03-05
  - Update: 升级 nginx-live-server 编译打包docker镜像指令

## 2022-12

- 2022-12-08
  - 1 Update: 更新编译指令，增加 with-ipv6
- 2022-11-25
  - 1 Update: 更新 README
- 2022-11-18
  - 1.1 Update: 更新编译指令
  - 1.2 Add: 增加一致性哈希模块
  - 1.3 Update: 优化镜像体积
- 2022-06-28
  - upgrade DEBIAN_VERSION from stretch-slim to bullseye-slim, from 9 to 11
  - remove redundant libs when no-ffmpeg

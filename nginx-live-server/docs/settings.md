# settings

- <https://github.com/winshining/nginx-http-flv-module/blob/master/README.CN.md>
- <https://github.com/arut/nginx-rtmp-module/blob/master/README.md>
- <https://github.com/arut/nginx-rtmp-module/wiki/Directives>

## record

```text
record
    off 关闭
    all 所有
    audio 音频
    video 视频

指定放置录制的flv文件的录制路径
record_path /tmp/rec;

设置录制文件后缀，默认 .flv
record_suffix -%Y-%m-%d-%T.flv;

设置录制文件名唯一，把时间戳附加到文件中
record_unique on;

设置录制文件最大大小
record_max_size 128M;
```

## live

```text
切换直播模式，一对多广播
live on;

切换交错模式，音视频在同一个RTMP块流
interleave on;

删除在指定时间内空闲的发布者连接??
drop_idle_publisher 10s;
```

## play

```text
从指定目录或 HTTP 位置播放 flv 或 mp4 文件

application vod {
    play /var/flvs;
}

application vod_http {
    play http://myserver.com/vod;
}

application vod_mirror {
    # try local location first, then access remote location
    play /var/local_mirror http://myserver.com/vod;
}
```

## hls

```text
切换 HLS
hls on;

设置 HLS 播放列表和片段目录
hls_path /tmp/hls;

设置 HLS 片段时长，默认5秒
hls_fragment 2s;

设置 HLS 播放列表长度，默认30秒
hls_playlist_length 1000m;

清理 HLS，默认开启
hls_cleanup off;

切换 HLS 嵌套模式，为每个流创建一个子目录
hls_nested on;

添加 HLS 变体条目
hls_variant _hi  BANDWIDTH=640000;

切换 HLS 连续模式 HLS 序列号从上次停止的地方开始。旧片段被保留
hls_continuous on;

使用系统时间戳命名 HLS 片
hls_fragment_naming system
```

## conf

### 在 HTTP 部分供播放

```text
http {
    ...
    server {
        ...
        location /hls {
            types {
                application/vnd.apple.mpegurl m3u8;
            }
            root /tmp;
            add_header Cache-Control no-cache;

            # To avoid issues with cross-domain HTTP requests (e.g. during development)
            add_header Access-Control-Allow-Origin *;
        }
    }
}
```

### 子流

```text
rtmp {
    server {
        listen 1935;

        application src {
            live on;

            exec ffmpeg -i rtmp://localhost/src/$name
              -c:a aac -b:a 32k  -c:v libx264 -b:v 128K -f flv rtmp://localhost/hls/$name_low
              -c:a aac -b:a 64k  -c:v libx264 -b:v 256k -f flv rtmp://localhost/hls/$name_mid
              -c:a aac -b:a 128k -c:v libx264 -b:v 512K -f flv rtmp://localhost/hls/$name_hi;
        }

        application hls {
            live on;

            hls on;
            hls_path /tmp/hls;
            hls_nested on;

            hls_variant _low BANDWIDTH=160000;
            hls_variant _mid BANDWIDTH=320000;
            hls_variant _hi  BANDWIDTH=640000;
        }
    }
}
```

### 统计数据

```text
http {
    server {
        location /stat {
            rtmp_stat all;
            rtmp_stat_stylesheet stat.xsl;
        }
        location /stat.xsl {
            root /path/to/stat/xsl/file;
        }
    }
}
```

### http-flv

```text
http {
    server {
        listen 8080;

        location /live {
            flv_live on;

            chunked_transfer_encoding on;                         #支持 'Transfer-Encoding: chunked' 方式回复

            add_header 'Access-Control-Allow-Origin' '*';         #添加额外的 HTTP 头
            add_header 'Access-Control-Allow-Credentials' 'true'; #添加额外的 HTTP 头
        }

        localtion /hls {
            types {
                application/vnd.apple.mpegurl m3u8;
                video/mp2t ts;
            }

            root /tmp/hls;
            add_header 'Cache-Control' 'no-cache';
        }

        location /stat {
            #推流播放和录制统计数据的配置

            rtmp_stat all;
            rtmp_stat_stylesheet stat.xsl;
        }

        location /stat.xsl {
            root /var/www/rtmp; #指定 stat.xsl 的位置
        }
    }
}

rtmp {
    server {
        listen 1985;

        application flv {
            live on;
        }

        application rtmp {
            live on;
        }

        application hls {
            live on;
            hls on;
            hls_path /tmp/hls;
        }
    }
}
```

- HTTP-FLV: <http://example.com:8080/live?port=1985&app=flv&stream=mystream>
- RTMP: <rtmp://example.com[:port]/rtmp/mystream>
- HLS: <rtmp://exampl.com[:port]/hls/mystream>
- HLS: <http://example.com[:port]/dir/streamname.m3u8>

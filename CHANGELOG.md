# CHANGELOG

[toc]

## 2023-06

- 2023-06-19
  - Add: panla/mysql

## 2023-05

- 2023-05-27
  - Upgrade: upgrade nginx from 1.22.0 to 1.24.0, nginx-http-flv-module from 1.2.10 to 1.2.11

## 2023-03

- 2023-03-21
  - Update: Nginx Compilation Modules
- 2023-03-19
  - Add: zlib openssl, pcre, source code into Compilation instructions
- 2023-03-09
  - Update: 更新 nginx-live-server Dockerfile 指令，更新默认配置
- 2023-03-07
  - Fix: 修复 nginx-live-server Dockerfile 错误
- 2023-03-05
  - Update: 升级 nginx-live-server 编译打包docker镜像指令

## 2022-11

- 2022-11-25
  - 1 Add: postgis

# README

Build opencv image base Python

## version

- opencv: 4.6.0
- Python: 3.9

## keywords

## modules

### extra modules

`opencv_contrib：4.6.0`

## pre

- 1，download the source code of opencv
- 2，download the source code of opencv-contrib

## dir

```text
.
├── CHANGELOG.md
├── Dockerfile
├── docs
│   └──
├── mirrors
│   └── sources.list
└── README.md
```

## build

```bash
docker build -f ./Dockerfile -t panla/opencv:python39-466 .
```

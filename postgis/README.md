# README

postgis

## desc

base from postgis/postgis:14-3.3

## ENV

- version: 14-3.3

## build

```bash
docker build -t panla/postgis:14-3.3 .

docker buildx build --platform linux/amd64,linux/arm64 --push --load -t panla/postgis:14-3.3 .
```
